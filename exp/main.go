package main

import (
	"authsite/models"
	"fmt"
)

var (
	host     = "localhost"
	port     = "5432"
	user     = "authsite"
	password = "123456"
	dbname   = "authdb"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	us, _ := models.NewUserService(psqlInfo)
	us.DestructiveReset()
	defer us.Close()

	user := models.User{
		Name:    "Bahman",
		Family:  "Shadmehr",
		Email:   "test@test.com",
		PhoneNo: "09388309605",
	}

	if err := us.Create(&user); err != nil {
		panic(err)
	}

	u, _ := us.ByEmail(user.Email)
	fmt.Println(u)

	us.Delete(1)

	_, e := us.ByID(1)
	fmt.Println(e)
}
