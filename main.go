package main

import (
	"authsite/controllers"
	"authsite/models"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	host     = "localhost"
	port     = "5432"
	user     = "authsite"
	password = "123456"
	dbname   = "authdb"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	us, err := models.NewUserService(psqlInfo)

	if err != nil {
		panic(err)
	}

	visitorsC := controllers.NewVisitors()
	usersC := controllers.NewUsers(us)

	r := mux.NewRouter()
	r.Handle("/", visitorsC.Home).Methods("GET")
	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")

	fs := http.FileServer(http.Dir("./views/statics/"))
	r.PathPrefix("/assets/").Handler(http.StripPrefix("", fs))

	http.ListenAndServe(":3000", r)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
