package views

import (
	"html/template"
	"net/http"
	"path/filepath"
)

// View Struct is a struct tohold view and related stuff
type View struct {
	Template *template.Template
	Layout   string
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := v.Render(w, nil); err != nil {
		panic(err)
	}
}

// Render is a function for rendering html template attachedto a view
func (v *View) Render(w http.ResponseWriter, data interface{}) error {
	w.Header().Set("Content-type", "text/html")
	return v.Template.ExecuteTemplate(w, v.Layout, data)
}

var (
	// VisitorsLayoutDir is the direction for visitor layout folder
	VisitorsLayoutDir string = "views/visitors/layouts/"
	// MainLayoutDir is the direction for main layout folder
	MainLayoutDir      string = "views/main/layouts/"
	templatesExtension string = ".gohtml"
	templateDir        string = "views/"
)

// NewView generates some views and gives them needed layouts
func NewView(layout string, layoutDir string, files ...string) *View {
	addTemplatePath(files)
	addTemplateExt(files)
	files = append(files, layoutFiles(layoutDir)...)
	t, err := template.ParseFiles(files...)
	if err != nil {
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}
}

func layoutFiles(path string) []string {
	files, err := filepath.Glob(path + "*" + templatesExtension)
	if err != nil {
		panic(err)
	}
	return files
}

// addTEmplatePath takes in a slice of string
// representing file paths, and it prepends
// the templateDir directory to each string
//
// Eg the input {"home"} would result in the output
// {"views/home"} if templateDir == "view/"
func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = templateDir + f
	}
}

// addTemplateExt takes in a slice of string
// representing file paths, and it appends
// the templateExtention to each string
//
// Eg the input {"home"} would result in the output
// {"home.gohtml"} if templateExtention == ".gohtml"
func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + templatesExtension
	}
}
