package controllers

import (
	"authsite/models"
	"authsite/views"
	"net/http"
)

// SignupForm is the type that is going to get the parsed
// data from our reqests
type SignupForm struct {
	Name                 string `schema:"name"`
	FamilyName           string `schema:"family_name"`
	Email                string `schema:"email"`
	PhoneNo              string `schema:"phone_no"`
	Password             string `schema:"password"`
	PasswordConfirmation string `schema:"password_confirmation"`
	CountryCode          string `schema:"country_code"`
}

// Users is the controllers that can generate user related views
type Users struct {
	NewView *views.View
	us      *models.UserService
}

// New function is used to render the signup page for a user
//
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	if err := u.NewView.Render(w, nil); err != nil {
		panic(err)
	}
}

// Create function is used to get the form data from signup form
// and proccess it for creatinga new usr
//
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var form SignupForm
	if err := ParseForm(r, &form); err != nil {
		panic(err)
	}

	user := models.User{
		Name:        form.Name,
		Family:      form.FamilyName,
		Email:       form.Email,
		PhoneNo:     form.PhoneNo,
		CountryCode: form.CountryCode,
	}
	if err := u.us.Create(&user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// NewUsers is the factory fo Users controller
func NewUsers(us *models.UserService) *Users {
	return &Users{
		NewView: views.NewView("main_layout", views.MainLayoutDir, "main/users/signup"),
		us:      us,
	}
}
