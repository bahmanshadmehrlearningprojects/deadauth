package controllers

import (
	"authsite/views"
)

// Visitors is the controller that is going to handle
type Visitors struct {
	Home *views.View
}

// NewVisitors is the factory fo Visitors controller
func NewVisitors() *Visitors {
	return &Visitors{
		Home: views.NewView("visitors_layout", views.VisitorsLayoutDir, "visitors/home"),
	}
}
