package models

import (
	"errors"

	"github.com/jinzhu/gorm"
	// This blank import is for connecting gorm to postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	// ErrNotFound When the resource does not exists in DB
	ErrNotFound = errors.New("models: resource not found")
	// ErrInvalidID is return when an invalid ID is given to a method like delete
	ErrInvalidID = errors.New("models: invalid ID")
)

// NewUserService is the factory of the UserService
func NewUserService(connectionInfo string) (*UserService, error) {
	db, err := gorm.Open("postgres", connectionInfo)
	if err != nil {
		return nil, err
	}

	db.LogMode(true)

	return &UserService{
		db: db,
	}, nil
}

//UserService is in charge of handling database related functions
type UserService struct {
	db *gorm.DB
}

// Close func Closes the UserService database
func (us *UserService) Close() error {
	return us.db.Close()
}

// ByID will look up users by theire id
// 1 - When a user is found - user, nil
// 2 - When we can not find the user - nil, ErrNotFound
// 3 - when a server error ecurs - nil, OtherError
func (us *UserService) ByID(id uint) (*User, error) {
	var user User
	db := us.db.Where("id = ?", id)
	err := first(db, &user)
	return &user, err
}

// ByEmail will look up users by theire id
// 1 - When a user is found - user, nil
// 2 - When we can not find the user - nil, ErrNotFound
// 3 - when a server error ecurs - nil, OtherError
func (us *UserService) ByEmail(email string) (*User, error) {
	var user User
	db := us.db.Where("email = ?", email)
	err := first(db, &user)
	return &user, err
}

// first will query using the provided gorm.db and it will find the first element of db and puts it in the destination
func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return err
}

// Create function adds a new user to database and return the gorm error in case of encountering an error
// It also fills out the fields like ID, CreatedDate and ...
func (us *UserService) Create(user *User) error {
	return us.db.Create(user).Error
}

// Delete will delete the object using its passed id
func (us *UserService) Delete(id uint) error {
	if id == 0 {
		return ErrInvalidID
	}
	user := User{Model: gorm.Model{ID: id}}
	err := us.db.Delete(&user).Error
	return err
}

// DestructiveReset resets the database and tables
func (us *UserService) DestructiveReset() {
	us.db.DropTableIfExists(&User{})
	us.db.AutoMigrate(&User{})
}

// Update will update the given user and updates all the fields
// givin in the user object
func (us *UserService) Update(user *User) error {
	return us.db.Save(user).Error
}

// User struct is going to handle all user related data
type User struct {
	gorm.Model
	Name        string
	Family      string
	Email       string `gorm:"not null;unique_index"`
	PhoneNo     string
	CountryCode string
}
