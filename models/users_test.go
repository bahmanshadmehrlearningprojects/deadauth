package models

import (
	"fmt"
	"testing"
)

func testingUserService() (*UserService, error) {
	const (
		host     = "localhost"
		port     = "5432"
		user     = "authsite"
		password = "123456"
		dbname   = "authdb"
	)

	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	us, err := NewUserService(psqlInfo)

	if err != nil {
		return nil, err
	}

	us.db.LogMode(false)

	// Clearing users table between tests
	us.DestructiveReset()

	return us, nil
}

func TestCreateUser(t *testing.T) {
	us, err := testingUserService()
	if err != nil {
		t.Fatal(err)
	}

	user := User{
		Name:    "Bahman",
		Family:  "Shadmehr",
		Email:   "test@test.com",
		PhoneNo: "09388309605",
	}
	err = us.Create(&user)
	if err != nil {
		t.Fatal(err)
	}

	if user.ID == 0 {
		t.Errorf("Expected ID > 0, Received %d", user.ID)
	}
}
